package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.bean.Venda;

public interface VendaRepository extends JpaRepository<Venda, Long> {

}
