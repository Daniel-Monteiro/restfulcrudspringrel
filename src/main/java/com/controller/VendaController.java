package com.controller;

import java.net.URI;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.model.bean.Pessoa;
import com.model.bean.Venda;
import com.repository.VendaRepository;

@RestController
@RequestMapping("/venda")
public class VendaController {
	
	@Autowired
	private VendaRepository vr;
	
	@GetMapping("/todas")
	public List<Venda> listaPessoas(){
		return vr.findAll();
	}
	
	@GetMapping("/buscaPorId/{id}")
	public Venda listaVendaById(@PathVariable("id") Long id){
		return vr.findById(id).get();
	}
	
	@DeleteMapping("/deletarPorId/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deletaVenda(@PathVariable("id") Long id) {
		vr.deleteById(id);
	}
	
	@PostMapping("/inserirVenda")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Venda> inserePessoa(@RequestBody Venda venda){
		Venda vendaSalva = vr.save(venda);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(vendaSalva.getId()).toUri();
		return ResponseEntity.created(uri).body(vendaSalva);
	}
	
	@PutMapping("/atualizaVenda/{id}")
	public ResponseEntity<Venda> atualizaPessoa(@PathVariable("id") long id, @RequestBody Venda venda) {
       
		Venda vendaAtualizada = vr.findById(id).get();
 
        vendaAtualizada.setDescricao(venda.getDescricao());
        vendaAtualizada.setDataPagamento(venda.getDataPagamento());
        vendaAtualizada.setDataVencimento(venda.getDataVencimento());
        
        vr.save(vendaAtualizada);
        return new ResponseEntity<Venda>(vendaAtualizada, HttpStatus.OK);
    }
}
