package com.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.model.bean.Pessoa;
import com.repository.PessoaRepository;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {
	
	@Autowired
	private PessoaRepository pr;
	
	@GetMapping("/todos")
	public List<Pessoa> listaPessoas(){
		return pr.findAll();
	}
	
	@GetMapping("/buscaPorId/{id}")
	public Pessoa listaPessoaById(@PathVariable("id") Long id){
		return pr.findById(id).get();
	}
	
	@DeleteMapping("/deletarPorId/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void deletaPessoa(@PathVariable("id") Long id) {
		pr.deleteById(id);
	}
	
	@PostMapping("/inserirPessoa")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Pessoa> inserePessoa(@RequestBody Pessoa pessoa){
		Pessoa pessoaSalva = pr.save(pessoa);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(pessoaSalva.getId()).toUri();
		return ResponseEntity.created(uri).body(pessoaSalva);
	}
	
	@PutMapping("/atualizaPessoa/{id}")
	public ResponseEntity<Pessoa> atualizaPessoa(@PathVariable("id") long id, @RequestBody Pessoa pessoa) {
       
		Pessoa pessoaAtualizada = pr.findById(id).get();
 
        pessoaAtualizada.setNome(pessoa.getNome());
        pessoaAtualizada.setIdade(pessoa.getIdade());
        
        pr.save(pessoaAtualizada);
        return new ResponseEntity<Pessoa>(pessoaAtualizada, HttpStatus.OK);
    }
 
}
